//
//  ViewController.swift
//  DemoCP
//
//  Created by phatdinh on 07/04/2022.
//

import UIKit
import MapKit

class CPModeViewController: UIViewController, ActionLoggerListener {
    var mobileDataSource: [String] = []
    var lastestLogIndexPath: IndexPath?
    
    lazy var locationManager: CLLocationManager = {
        var manager = CLLocationManager()
        manager.distanceFilter = 10
        manager.desiredAccuracy = kCLLocationAccuracyBest
        return manager
    }()
    
    lazy var tableView: UITableView = {
        let tableView = UITableView()
        tableView.translatesAutoresizingMaskIntoConstraints = false
        return tableView
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "CarPlay Manager"
        
        ActionLogger.shared.addListener(self)
        
        tableView.delegate = self
        tableView.dataSource = self
        self.view.addSubview(tableView)
        
        NSLayoutConstraint.activate([
            tableView.leftAnchor.constraint(equalTo: self.view.leftAnchor),
            tableView.rightAnchor.constraint(equalTo: self.view.rightAnchor),
            tableView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor),
            tableView.topAnchor.constraint(equalTo: self.view.topAnchor)
        ])
        
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
    }

    func onAddDataFinished(newData: String) {
        self.mobileDataSource.append(newData)
        tableView.reloadData()
        guard let lastestLogIndexPath = IndexPath(row: self.mobileDataSource.count - 1, section: 0) as? IndexPath else {
            return
        }
        // Scroll to the End
        self.tableView.scrollToRow(at: lastestLogIndexPath, at: .bottom, animated: true)
        self.lastestLogIndexPath = nil
    }
}

extension CPModeViewController: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager,
                         didChangeAuthorization status: CLAuthorizationStatus) {
        
        if status == .authorizedWhenInUse || status == .authorizedAlways {
            locationManager.startUpdatingLocation()
        }
    }
}

extension CPModeViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return mobileDataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        cell.textLabel?.text = mobileDataSource[indexPath.row]
        if indexPath.row == mobileDataSource.count - 1 {
            self.lastestLogIndexPath = indexPath
        }
        
        return cell
    }
}
