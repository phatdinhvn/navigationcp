//
//  ActionLogger.swift
//  DemoCP
//
//  Created by phatdinh on 07/04/2022.
//

import Foundation

protocol ActionLoggerListener {
    func onAddDataFinished(newData: String)
}

class ActionLogger {
    static let shared: ActionLogger = ActionLogger()
    
    var dataSource: [String] = []
    private var listeners: [ActionLoggerListener] = []
    
    
    private init() {}
    
    func addData(_ data: String) {
        dataSource.append(data)
        
        // notify data is added
        if dataSource.contains(data) {
            for listener in listeners {
                listener.onAddDataFinished(newData: data)
            }
        }
    }
    
    func addListener(_ listener: ActionLoggerListener) {
        listeners.append(listener)
    }
}
