//
//  LocationManager.swift
//  DemoCP
//
//  Created by phatdinh on 07/04/2022.
//

import Foundation
import MapKit

enum MoveDirecttion {
    case UP
    case DOWN
    case RIGHT
    case LEFT
}

enum MoveRatio: CGFloat {
    case SMALL = 0.01
    case MEDIUM = 0.1
    case LARGE = 0.5
}

protocol LocationManagerDelegate {
    func onSearchKeywordFinished(_ resultList: [MKMapItem])
}

extension LocationManagerDelegate {
    func onSearchKeywordFinished(_ resultList: [MKMapItem]) {}
}

class LocationManager {
    static let shared: LocationManager = LocationManager()
    private var listeners: [LocationManagerDelegate] = []
    
    var cpMapViewController: CarPlayViewController?
    
    var routing: MKRoute?
    
    private init() {}
    
    func addListener(_ listener: LocationManagerDelegate) {
        listeners.append(listener)
    }
    
    func zoomIn() {
        guard let cpMapViewController = cpMapViewController else {
            return
        }
        let cameraZoom = MKMapView.CameraZoomRange(minCenterCoordinateDistance: 35.555, maxCenterCoordinateDistance: 135.555)
        cpMapViewController.mapView.setCameraZoomRange(cameraZoom, animated: true)
        
    }
    
    func panMap(_ direction: MoveDirecttion) {
        guard let cpMapViewController = cpMapViewController else {
            return
        }
        
        let centerPoint = cpMapViewController.mapView.center
        var nextPoint = cpMapViewController.mapView.center
        let mapView = cpMapViewController.mapView
        switch direction {
        case .UP:
            nextPoint = CGPoint(x: CGFloat(centerPoint.x), y: centerPoint.y - (mapView?.frame.height ?? 0.0) * MoveRatio.MEDIUM.rawValue)
        case .DOWN:
            nextPoint = CGPoint(x: CGFloat(centerPoint.x), y: centerPoint.y + (mapView?.frame.height ?? 0.0) * MoveRatio.MEDIUM.rawValue)
        case .LEFT:
            nextPoint = CGPoint(x: centerPoint.x - (mapView?.frame.width ?? 0.0) * MoveRatio.MEDIUM.rawValue, y: CGFloat(centerPoint.y))
        case .RIGHT:
            nextPoint = CGPoint(x: centerPoint.x + (mapView?.frame.width ?? 0.0) * MoveRatio.MEDIUM.rawValue, y: CGFloat(centerPoint.y))

        }
        let newCoordinate = cpMapViewController.mapView.convert(nextPoint, toCoordinateFrom: cpMapViewController.view)

        cpMapViewController.mapView.setCenter(newCoordinate, animated: true)
    }
    
    func resetCurrentLocation(for mapView: MKMapView) {
        let locationManager = CLLocationManager()
        locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
        locationManager.startUpdatingLocation()
        
        mapView.setCenter(locationManager.location!.coordinate, animated: true)
    }
    
    func searchWithKeyword(mapView: MKMapView, _ keyword: String) {
        mapView.removeAnnotations(mapView.annotations)
        let request = MKLocalSearch.Request()
        request.naturalLanguageQuery = keyword
        request.region = mapView.region
        let search = MKLocalSearch(request: request)
        
        search.start { response, _ in
            for listener in self.listeners {
                listener.onSearchKeywordFinished(response == nil ? [] : response!.mapItems)
            }
            search.cancel()
        }
        
    }
    
    func scroll(mapView: MKMapView, to placeMark: MKPlacemark) {
        mapView.addAnnotation(Pin(title: placeMark.name, locationName: placeMark.name, discipline: "Test", coordinate: placeMark.coordinate))
//        mapView.showAnnotations(mapView.annotations, animated: true)
        mapView.setCenter(placeMark.coordinate, animated: true)
    }
    
    func searchRoute(mapView: MKMapView!, source: MKMapItem!, destination: MKMapItem!, completionHandler: @escaping (Bool) -> Void) {
        let request = MKDirections.Request()
        request.source = source
        request.destination = destination
        
        let direction = MKDirections(request: request)
        direction.calculate { response, error in
            guard let response = response else {
                print("Error: \(error?.localizedDescription ?? "No error specified").")
                return
            }
            
            self.routing = response.routes[0]
            mapView.addOverlay(self.routing!.polyline, level: .aboveRoads)
            mapView.setRegion(MKCoordinateRegion(self.routing!.polyline.boundingMapRect), animated: true)
            completionHandler(true)
        }
    }
    
    func sendToCar(_ annotation: MKAnnotation) {
        guard let cpMapViewController = cpMapViewController else {
            return
        }
        
        let mapView = cpMapViewController.mapView
        
        mapView?.addAnnotation(annotation)
        mapView?.setCenter(annotation.coordinate, animated: true)
        ActionLogger.shared.addData("Pushed coordinate to CarPlay")
    }
    
    func sendRoutingToCar() {
        guard let cpMapViewController = cpMapViewController,
        let mapView = cpMapViewController.mapView else {
            return
        }
        
        mapView.addOverlay(self.routing!.polyline, level: .aboveRoads)
        mapView.setRegion(MKCoordinateRegion(self.routing!.polyline.boundingMapRect), animated: true)
        ActionLogger.shared.addData("Pushed route to CarPlay")
    }
}
