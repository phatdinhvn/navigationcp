//
//  MapViewController.swift
//  DemoCP
//
//  Created by Phat Dinh on 11/06/2022.
//

import Foundation
import UIKit
import MapKit

class MapViewController: UIViewController {
    
    let cellId = "cellId"
    let annotationViewId = "annotationViewId"
    
    var selectedMapItem: MKMapItem? {
        didSet {
            self.buttonStack.isHidden = nil == selectedMapItem
        }
    }
        
    lazy var searchBar: UISearchBar = {
        let searchBar = UISearchBar()
        
        searchBar.translatesAutoresizingMaskIntoConstraints = false
        return searchBar
    }()
    
    lazy var mapView: MKMapView = {
        let mapView = MKMapView()
        mapView.translatesAutoresizingMaskIntoConstraints = false
        mapView.register(MKAnnotationView.self, forAnnotationViewWithReuseIdentifier: annotationViewId)
        return mapView
    }()
    
    lazy var tableView: UITableView = {
        let tableView = UITableView()
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: cellId)
        return tableView
    }()
    
    lazy var heightTableView: NSLayoutConstraint = {
        let heightTableView = tableView.heightAnchor.constraint(equalToConstant: 0)
        heightTableView.isActive = true
        return heightTableView
    }()
    
    lazy var routeButton: UIButton = {
        let button = UIButton(type: .system)
        button.layer.cornerRadius = 5
        button.backgroundColor = .black
        let action = UIAction { action in
            self.searchRouteButtonTapped()
        }
        button.setTitle("Route", for: .normal)
        button.setTitleColor(UIColor.white, for: .normal)
        button.addAction(action, for: .touchUpInside)
        return button
    }()
    
    lazy var sendToCar: UIButton = {
        let button = UIButton(type: .system)
        button.layer.cornerRadius = 5
        button.backgroundColor = .white
        let action = UIAction { action in
            self.sendToCarButtonTapped()
        }
        button.isHidden = true
        button.setTitle("Send to Car", for: .normal)
        button.setTitleColor(UIColor.black, for: .normal)
        button.addAction(action, for: .touchUpInside)
        return button
    }()
    
    lazy var buttonStack: UIStackView = {
        let stack = UIStackView(arrangedSubviews: [routeButton, sendToCar])
        stack.isHidden = true
        stack.distribution = .fillEqually
        stack.alignment = .fill
        stack.translatesAutoresizingMaskIntoConstraints = false
        stack.axis = .horizontal
        stack.spacing = 10
        return stack
    }()
    
    var matchingItems: [MKMapItem] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Setup Delegate
        self.searchBar.delegate = self
        self.tableView.delegate = self
        self.tableView.dataSource = self
        
        LocationManager.shared.addListener(self)
        
        // Setup layout
        self.title = "Map"
        self.navigationController?.navigationBar.backgroundColor = UIColor(named: "BackgroundColor")
        self.view.backgroundColor = UIColor(named: "BackgroundColor")
        
        self.view.addSubview(self.searchBar)
        NSLayoutConstraint.activate([
            searchBar.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 16),
            searchBar.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: -16),
            searchBar.topAnchor.constraint(equalTo: self.view.topAnchor, constant: 100)
        ])
        
        self.view.addSubview(self.mapView)
        NSLayoutConstraint.activate([
            mapView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor),
            mapView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor),
            mapView.topAnchor.constraint(equalTo: self.searchBar.bottomAnchor),
            mapView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor)
        ])
        
        mapView.delegate = self
        
        self.view.addSubview(self.buttonStack)
        NSLayoutConstraint.activate([
            buttonStack.centerXAnchor.constraint(equalTo: self.view.centerXAnchor),
            buttonStack.widthAnchor.constraint(equalTo: self.view.widthAnchor, multiplier: 0.8),
            buttonStack.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: -30),
            buttonStack.heightAnchor.constraint(equalToConstant: 56)
        ])
        
        self.view.addSubview(self.tableView)
        NSLayoutConstraint.activate([
            tableView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor),
            tableView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor),
            tableView.topAnchor.constraint(equalTo: self.mapView.topAnchor),
        ])
        
        
    }
    
    private func searchRouteButtonTapped() {
        if let _ = selectedMapItem {
            self.sendToCar.isHidden = true
            let locationManager = CLLocationManager()
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
            let source = MKMapItem(placemark: MKPlacemark(coordinate: locationManager.location!.coordinate))
            LocationManager.shared.searchRoute(mapView: self.mapView, source: source, destination: selectedMapItem) { success in
                self.sendToCar.isHidden = !success
            }
        }
    }
    
    private func sendToCarButtonTapped() {
        LocationManager.shared.sendRoutingToCar()
    }
}

extension MapViewController: UISearchBarDelegate {
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        // Search/Enter button clicked
        searchBar.resignFirstResponder()
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        LocationManager.shared.searchWithKeyword(mapView: self.mapView, searchBar.text ?? "")
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText.isEmpty {
            self.matchingItems.removeAll()
            self.tableView.reloadData()
            self.selectedMapItem = nil
            self.mapView.removeAnnotations(self.mapView.annotations)
        }
    }
}

extension MapViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        heightTableView.constant = CGFloat(matchingItems.count) * 44.0
        return matchingItems.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId)
        
        cell?.textLabel?.text = self.matchingItems[indexPath.row].name
        
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let selectedMapItem = matchingItems[indexPath.row].placemark
        LocationManager.shared.scroll(mapView: self.mapView, to: selectedMapItem)
        self.selectedMapItem = matchingItems[indexPath.row]
        self.heightTableView.constant = 0
    }
}

extension MapViewController: MKMapViewDelegate {
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        let annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: annotationViewId)
        annotationView?.annotation = annotation
        annotationView?.image = Image.pin
        annotationView?.canShowCallout = true
        return annotationView
    }
    
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        if let annotation = view.annotation {
            LocationManager.shared.sendToCar(annotation)
        }
    }
    
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        let renderer = MKPolylineRenderer(overlay: overlay)
        renderer.lineWidth = 5
        renderer.strokeColor = .blue
        return renderer
    }
}

extension MapViewController: LocationManagerDelegate {
    func onSearchKeywordFinished(_ resultList: [MKMapItem]) {
        self.matchingItems = resultList
        self.tableView.reloadData()
    }
}
