//
//  CarPlayViewController.swift
//  DemoCP
//
//  Created by phatdinh on 11/04/2022.
//

import MapKit
import UIKit

class CarPlayViewController: UIViewController {
    @IBOutlet weak var mapView: MKMapView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        LocationManager.shared.resetCurrentLocation(for: mapView)
        self.mapView.delegate = self
        print("viewDidLoad")
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        print("viewDidAppear")
    }
}

extension CarPlayViewController: MKMapViewDelegate {
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        let renderer = MKPolylineRenderer(overlay: overlay)
        renderer.lineWidth = 5
        renderer.strokeColor = .blue
        return renderer
    }
}
