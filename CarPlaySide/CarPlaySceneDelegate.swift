//
//  CarPlaySceneDelegate.swift
//  DemoCP
//
//  Created by phatdinh on 07/04/2022.
//

import CarPlay

class CarPlaySceneDelegate: UIResponder, UIApplicationDelegate , CPTemplateApplicationSceneDelegate {
    var interfaceController: CPInterfaceController?
    var carWindow: CPWindow?
    
    
    func templateApplicationScene(_ templateApplicationScene: CPTemplateApplicationScene, didConnect interfaceController: CPInterfaceController, to window: CPWindow) {
        self.interfaceController = interfaceController
        self.carWindow = window
                
        guard let carPlayVC = Bundle.main.loadNibNamed("CarPlayViewController", owner: self, options: nil)?.first as? CarPlayViewController else {
            return
        }

        window.rootViewController = carPlayVC
        LocationManager.shared.cpMapViewController = carPlayVC
        
        let mapTemplate = CPMapTemplate()
        mapTemplate.mapDelegate = self
        mapTemplate.automaticallyHidesNavigationBar = true
        let leftButton = CPBarButton(title: "Reset") { button in
            ActionLogger.shared.addData(button.title ?? "Reset")
            print("Reset tapped")
            
            LocationManager.shared.resetCurrentLocation(for: carPlayVC.mapView)
        }
        mapTemplate.leadingNavigationBarButtons = [leftButton]
        
        let rightButton = CPBarButton(title: "Pan") { button in
            ActionLogger.shared.addData(button.title ?? "Pan/Unpan")
            if button.title != "Pan" {
                button.title = "Pan"
                mapTemplate.dismissPanningInterface(animated: true)
                
            } else {
                button.title = "Unpan"
                mapTemplate.showPanningInterface(animated: true)
            }
        }
        mapTemplate.trailingNavigationBarButtons = [rightButton]
        
        let zoomInMapButton = CPMapButton { button in
            do {
                var coordinateSpan = carPlayVC.mapView?.region.span
                if let span = coordinateSpan {
                    coordinateSpan = MKCoordinateSpan(latitudeDelta: span.latitudeDelta - span.latitudeDelta * 0.1, longitudeDelta: span.longitudeDelta - span.longitudeDelta * 0.1)
                }
                
                carPlayVC.mapView.setRegion(MKCoordinateRegion(center: carPlayVC.mapView.centerCoordinate, span: coordinateSpan ?? MKCoordinateSpan(latitudeDelta: 0, longitudeDelta: 0)), animated: true)
            }
        }
        let zoomOutMapButton = CPMapButton { button in
            do {
                var coordinateSpan = carPlayVC.mapView?.region.span
                if let span = coordinateSpan {
                    coordinateSpan = MKCoordinateSpan(latitudeDelta: span.latitudeDelta + span.latitudeDelta * 0.1, longitudeDelta: span.longitudeDelta + span.longitudeDelta * 0.1)
                    
                }
                carPlayVC.mapView.setRegion(MKCoordinateRegion(center: carPlayVC.mapView.centerCoordinate, span: coordinateSpan ?? MKCoordinateSpan(latitudeDelta: 0, longitudeDelta: 0)), animated: true)
            }
        }
        
        zoomInMapButton.image = .add
        zoomOutMapButton.image = .remove
        
        mapTemplate.mapButtons = [zoomInMapButton, zoomOutMapButton]
        
        self.interfaceController?.setRootTemplate(mapTemplate, animated: true, completion: nil)
    }
    
    func templateApplicationScene(_ templateApplicationScene: CPTemplateApplicationScene, didDisconnectInterfaceController interfaceController: CPInterfaceController) {
        self.interfaceController = nil
    }
    
    
}

extension CarPlaySceneDelegate: CPMapTemplateDelegate {
    func mapTemplateDidBeginPanGesture(_ mapTemplate: CPMapTemplate) {
        print("Did Begin Pan")
    }
    
    /**
     Touch on UP/LEFT/DOWN/RIGHT pan button on CP
     */
    func mapTemplate(_ mapTemplate: CPMapTemplate, panWith direction: CPMapTemplate.PanDirection) {
        switch direction {
        case .down:
            ActionLogger.shared.addData("DOWN")
            LocationManager.shared.panMap(.DOWN)
        case .up:
            ActionLogger.shared.addData("UP")
            LocationManager.shared.panMap(.UP)
        case .left:
            ActionLogger.shared.addData("LEFT")
            LocationManager.shared.panMap(.LEFT)
        case .right:
            ActionLogger.shared.addData("RIGHT")
            LocationManager.shared.panMap(.RIGHT)
        default:
            ActionLogger.shared.addData("Unknown")
        }
    }
}

extension CarPlaySceneDelegate: CPTemplateApplicationDashboardSceneDelegate {
    func templateApplicationDashboardScene(_ templateApplicationDashboardScene: CPTemplateApplicationDashboardScene, didConnect dashboardController: CPDashboardController, to window: UIWindow) {
        guard let carPlayVC = Bundle.main.loadNibNamed("CarPlayViewController", owner: self, options: nil)?.first as? UIViewController else {
            return
        }

        window.rootViewController = carPlayVC
        
        // Setting dashboard button for CP
        let aButton = CPDashboardButton(
            titleVariants: ["Reset"],
            subtitleVariants: ["Back to me"],
            image: UIImage()) { (button) in
                let locationManager = CLLocationManager()
                locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
                locationManager.startUpdatingLocation()
                
                (carPlayVC as! CarPlayViewController).mapView.setCenter(locationManager.location!.coordinate, animated: true)

        }

        dashboardController.shortcutButtons = [aButton]
        
    }
    
    
}
